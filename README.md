Rafa y Dani

# Money decomposition
> Easy tests, simpler code!

## Exercise
Given an amount of money between 1 and 1000000, write a program that outputs the best cash and notes convination so you don't have a pocket full of trashy iron.
You can use notes of 500, 200, 100, 50, 20, 10 and 5 and coins of 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01.

## Example
```
> convert(5784.29)
< [
      '500'  => 11,
      '200'  => 1,
      '50'   => 1,
      '20'   => 1,
      '10'   => 1,
      '2'    => 2,
      '0.2'  => 1,
      '0.05' => 1,
      '0.02' => 2,
  ]
```

## Contract
```public Mpwar\MoneyDecomposition\Converter::convert(double $amount) : int[]```

## Goal
You need to implement the code as much SOLIDified as possible.